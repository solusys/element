package com.solusys.client.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import com.solusys.client.ui.components.PageInfo;

public class ContactData {

	private List<Contact> contacts;

	public ContactData(int numContacts) {
		this.contacts = generateRandomContacts(numContacts);
	}

	public List<Contact> generateRandomContacts(int numContacts) {
		List<Contact> contacts = new ArrayList<Contact>();
		Random random = new Random();

		for (int i = 0; i < numContacts; i++) {
			String firstName = generateRandomName(random);
			String lastName = generateRandomName(random);
			String email = "email" + i + "@example.com";
			contacts.add(new Contact(firstName, lastName, email));
		}

		return contacts;
	}

	private String generateRandomName(Random random) {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		int nameLength = random.nextInt(6) + 5; // Generate a name with 5-10 characters
		StringBuilder nameBuilder = new StringBuilder();

		for (int i = 0; i < nameLength; i++) {
			int index = random.nextInt(alphabet.length());
			char c = alphabet.charAt(index);
			nameBuilder.append(c);
		}

		return nameBuilder.toString();
	}

	public List<Contact> getPage22(PageInfo pageInfo) {
		// Create a copy of the contacts list
		List<Contact> sortedContacts = new ArrayList<>(contacts);

		// Sort the list based on the sort column and order specified in pageInfo
		// You can replace this with the previous sorting logic if you want
		sortedContacts.sort(Comparator.comparing(Contact::getFirstName));

		// Calculate the start index and end index of the requested page
		int startIndex = pageInfo.getPageNumber() * pageInfo.getPageSize();
		int endIndex = Math.min(startIndex + pageInfo.getPageSize(), sortedContacts.size());

		// Return the sublist containing the contacts for the requested page
		return sortedContacts.subList(startIndex, endIndex);
	}

	public List<Contact> getPage(PageInfo pageInfo) {
		int pageSize = pageInfo.getPageSize();
		int pageNumber = pageInfo.getPageNumber();
		String sortColumn = pageInfo.getSortBy();
		boolean ascending = pageInfo.isAscending();

		// Sort the contacts list based on the sortColumn and ascending flag
		contacts.sort((contact1, contact2) -> {
			int comparison;
			switch (sortColumn) {
			case "firstName":
				comparison = compareWithNullHandling(contact1.getFirstName(), contact2.getFirstName());
				break;
			case "lastName":
				comparison = compareWithNullHandling(contact1.getLastName(), contact2.getLastName());
				break;
			case "email":
			default:
				comparison = compareWithNullHandling(contact1.getEmail(), contact2.getEmail());
				break;
			}
			return ascending ? comparison : -comparison;
		});

		// Calculate the start and end index for the page
		int startIndex = pageNumber * pageSize;
		int endIndex = Math.min(startIndex + pageSize, contacts.size());

		// Return the sublist for the page
		return contacts.subList(startIndex, endIndex);
	}

	private int compareWithNullHandling(String value1, String value2) {
		if (value1 == null && value2 == null) {
			return 0;
		}
		if (value1 == null) {
			return -1;
		}
		if (value2 == null) {
			return 1;
		}
		return value1.compareTo(value2);
	}

}