package com.solusys.client.ui.components;

import elemental2.dom.CustomEventInit;
import elemental2.dom.Event;
import jsinterop.annotations.JsConstructor;

public class RowSelectedEvent<T> extends Event {
	public static final String TYPE = "rowselected";
	private T rowData;
	private boolean doubleClick;

	@JsConstructor
	public RowSelectedEvent(T rowData, boolean doubleClick) {
		super(TYPE, CustomEventInit.create());
		this.rowData = rowData;
		this.doubleClick = doubleClick;
	}

	public T getRowData() {
		return rowData;
	}

	public boolean isDoubleClick() {
		return doubleClick;
	}
}
