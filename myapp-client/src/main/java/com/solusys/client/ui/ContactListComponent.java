package com.solusys.client.ui;

import java.util.List;

import com.solusys.client.service.Contact;
import com.solusys.client.service.ContactData;
import com.solusys.client.ui.components.PageInfo;
import com.solusys.client.ui.components.Table;
import com.solusys.client.ui.router.AbstractRouteComponent;

import elemental2.dom.DomGlobal;

public class ContactListComponent extends AbstractRouteComponent {
	// private final Element element;

	public ContactListComponent() {
		super(Constants.CONTACT_LIST);

		this.element = DomGlobal.document.createElement("div");

		ContactData contactData = new ContactData(100);
		// List<Contact> contacts = getContacts();
		// List<String> columns = Arrays.asList("firstName", "lastName", "email");

		Table<Contact> table = new Table<Contact>(contactData.generateRandomContacts(100));
		table.addColumn("firstName", c -> c.getFirstName());
		table.addColumn("lastName", c -> c.getLastName());
		table.addColumn("email", c -> c.getEmail());
		
		

		
//		PageInfo pageInfo = new PageInfo();
//		pageInfo.setAscending(true);
//		pageInfo.setPageNumber(1);
//		pageInfo.setPageSize(13);
//		pageInfo.setSortBy("firstName");
//		
//		table.setItems(contactData.getPage(pageInfo));

		// table.addColumn("email2", c -> c.getEmail() + "/" + c.getEmail());
//
//		table.addRowSelectedEventListener(event -> {
//			if (event.isDoubleClick()) {
//				Contact selectedContact = event.getRowData();
//				DomGlobal.console
//						.log("Clicked twice: " + selectedContact.getFirstName() + " " + selectedContact.getLastName());
//			} else {
//				Contact selectedContact = event.getRowData();
//				DomGlobal.console
//						.log("Clicked once: " + selectedContact.getFirstName() + " " + selectedContact.getLastName());
//			}
//		});

		table.addPageChangedEventListener(event -> {
//			DomGlobal.console.log("==============>");
//			DomGlobal.console.log(event.getPageInfo().toString());
			List<Contact> list = contactData.getPage(event.getPageInfo());
//			DomGlobal.console.log(list.size());
			table.setItems(list);
		});

		element.appendChild(table.createTableElement());
		element.appendChild(table.getPaginationContainer());
	}
}
