package com.solusys.client.ui.components;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

@JsType(isNative = true, namespace = JsPackage.GLOBAL)
@FunctionalInterface
public interface PageChangedEventListener {
	void onInvoke(PageChangedEvent event);
}