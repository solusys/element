package com.solusys.client.ui.router;

import elemental2.dom.Element;

public interface Route {
	
	
	String getPath();

	void render(Element container);
}