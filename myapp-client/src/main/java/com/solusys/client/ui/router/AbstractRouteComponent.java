package com.solusys.client.ui.router;

import com.solusys.client.ui.Component;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

public abstract class AbstractRouteComponent implements Route, Component {
	private String path;
	protected Element element;

	public AbstractRouteComponent(String path) {
		element = DomGlobal.document.createElement("div");
		this.path = path;
	}

	public void render(Element container) {
		container.appendChild(getElement());
	};

	public Element getElement() {
		return element;
	}

	public String getPath() {
		return path;
	}

}
