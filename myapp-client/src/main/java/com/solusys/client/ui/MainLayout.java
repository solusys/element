package com.solusys.client.ui;

import elemental2.dom.CSSProperties;
import elemental2.dom.DomGlobal;


//public abstract class MainLayout {

//
//	private HTMLElement navElement;
//	private HTMLDivElement contentElement;
//
//	public MainLayout() {
//		// create navigation menu
//		navElement = (HTMLElement) DomGlobal.document.createElement("nav");
//		// navElement.textContent = "Navigation Menu";
//		// Set attributes and properties for the nav element
//		navElement.setAttribute("class", "navbar");
//		// navElement.textContent = "Navigation Bar";
//
//		// create content area
//		contentElement = (HTMLDivElement) DomGlobal.document.createElement("div");
//		// contentElement.textContent = "Content";
//
//		// create main container element
//		HTMLDivElement mainElement = (HTMLDivElement) DomGlobal.document.createElement("div");
//		mainElement.style.margin = CSSProperties.MarginUnionType.of("5px");
//		mainElement.classList.add("horizontal-layout");
//
//		mainElement.appendChild(navElement);
//		mainElement.appendChild(contentElement);
//
//		HTMLMetaElement viewport = (HTMLMetaElement) DomGlobal.document.createElement("meta");
//		viewport.setAttribute("name", "viewport");
//		viewport.setAttribute("content", "width=device-width, initial-scale=1, minimum-scale=1");
//
//		DomGlobal.document.head.appendChild(viewport);
//
//		// add main container element to page
//		DomGlobal.document.body.appendChild(mainElement);
//	}
//
//	public Element getNavElement() {
//		return navElement;
//	}
//
//	public Element getContentElement() {
//		return contentElement;
//	}
//
//}
//
//
//

import elemental2.dom.HTMLTableElement;
import elemental2.dom.HTMLTableRowElement;
import elemental2.dom.HTMLTableCellElement;

public class MainLayout {

	private HTMLTableElement container;
	private HTMLTableRowElement headerRow;
	private HTMLTableCellElement headerCell;
	private HTMLTableRowElement contentRow;
	private HTMLTableCellElement leftCell;
	private HTMLTableCellElement rightCell;
	private HTMLTableRowElement footerRow;
	private HTMLTableCellElement footerCell;

	public MainLayout() {
		container = (HTMLTableElement) DomGlobal.document.createElement("table");
		container.style.width = CSSProperties.WidthUnionType.of("100%");
		container.style.height = CSSProperties.HeightUnionType.of("100%");

		headerRow = (HTMLTableRowElement) container.insertRow();
		headerCell = (HTMLTableCellElement) headerRow.insertCell(0);
		headerCell.colSpan = 2;
		headerCell.style.backgroundColor = "#ccc";
		headerCell.style.padding = CSSProperties.PaddingUnionType.of("10px"); // "10px";
		headerCell.textContent = "Header";

		contentRow = (HTMLTableRowElement) container.insertRow();
		contentRow.style.width = CSSProperties.WidthUnionType.of("100%");
		contentRow.style.height = CSSProperties.HeightUnionType.of("100%");

		leftCell = (HTMLTableCellElement) contentRow.insertCell(0);
		leftCell.style.width = CSSProperties.WidthUnionType.of("20%");// "20%";
		leftCell.style.backgroundColor = "#eee";
		leftCell.style.padding = CSSProperties.PaddingUnionType.of("10px");// "10px";
		//leftCell.textContent = "Navigation";

		rightCell = (HTMLTableCellElement) contentRow.insertCell(1);
		rightCell.style.width = CSSProperties.WidthUnionType.of("80%");// "80%";
		rightCell.style.backgroundColor = "#fff";
		rightCell.style.padding = CSSProperties.PaddingUnionType.of("10px");// "10px";
		//rightCell.textContent = "Content";

		footerRow = (HTMLTableRowElement) container.insertRow();
		footerCell = (HTMLTableCellElement) footerRow.insertCell(0);
		footerCell.colSpan = 2;
		footerCell.style.backgroundColor = "#ccc";
		footerCell.style.padding = CSSProperties.PaddingUnionType.of("10px");// "10px";
		footerCell.textContent = "Footer";

		DomGlobal.document.body.appendChild(container);
	}

	public HTMLTableElement getContainer() {
		return container;
	}

	public HTMLTableRowElement getHeaderRow() {
		return headerRow;
	}

	public void setHeaderRow(HTMLTableRowElement headerRow) {
		this.headerRow = headerRow;
	}

	public HTMLTableCellElement getHeaderCell() {
		return headerCell;
	}

	public void setHeaderCell(HTMLTableCellElement headerCell) {
		this.headerCell = headerCell;
	}

	public HTMLTableRowElement getContentRow() {
		return contentRow;
	}

	public void setContentRow(HTMLTableRowElement contentRow) {
		this.contentRow = contentRow;
	}

	public HTMLTableCellElement getLeftCell() {
		return leftCell;
	}

	public void setLeftCell(HTMLTableCellElement leftCell) {
		this.leftCell = leftCell;
	}

	public HTMLTableCellElement getRightCell() {
		return rightCell;
	}

	public void setRightCell(HTMLTableCellElement rightCell) {
		this.rightCell = rightCell;
	}

	public HTMLTableRowElement getFooterRow() {
		return footerRow;
	}

	public void setFooterRow(HTMLTableRowElement footerRow) {
		this.footerRow = footerRow;
	}

	public HTMLTableCellElement getFooterCell() {
		return footerCell;
	}

	public void setFooterCell(HTMLTableCellElement footerCell) {
		this.footerCell = footerCell;
	}

	public void setContainer(HTMLTableElement container) {
		this.container = container;
	}
}
