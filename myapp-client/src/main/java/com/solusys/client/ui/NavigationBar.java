package com.solusys.client.ui;

import com.solusys.client.ui.router.AbstractRouteComponent;
import com.solusys.client.ui.router.Router;

import elemental2.dom.CSSProperties;
import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLAnchorElement;
import elemental2.dom.HTMLDivElement;

public class NavigationBar {

	private HTMLDivElement navBar;
	private Router router;

	public NavigationBar(Element appElement) {
		this.navBar = buildLayout();
		this.router = new Router(appElement);
	}

	private HTMLDivElement buildLayout() {
		HTMLDivElement verticalLayout = (HTMLDivElement) DomGlobal.document.createElement("div");
		verticalLayout.style.display = "flex";
		verticalLayout.style.flexDirection = "column";
		verticalLayout.style.justifyContent = "flex-start";
		verticalLayout.style.alignItems = "stretch";
		return verticalLayout;
	}

	public HTMLDivElement getElement() {
		return navBar;
	}

	private void addLink(String name, String path) {
		HTMLAnchorElement link = (HTMLAnchorElement) DomGlobal.document.createElement("a");

		link.style.margin = CSSProperties.MarginUnionType.of("5px");
		link.style.display = "inline-block";
		link.style.padding = CSSProperties.PaddingUnionType.of("10px");// "10px";
		link.style.backgroundColor = "lightblue";
		link.style.borderRadius = CSSProperties.BorderRadiusUnionType.of("5px");// "5px";
		link.style.textDecoration = "none";
		link.style.color = "white";

		link.href = "#";
		link.textContent = name;
		link.addEventListener("click", event -> {
			router.go(path);
			event.preventDefault();
		});
		navBar.appendChild(link);
	}

	public void addComponent(String name, AbstractRouteComponent homeComponent) {
		router.addRoute(name, homeComponent);
		addLink(name, homeComponent.getPath());
	}

	public void init() {
		router.init();
	}

}
