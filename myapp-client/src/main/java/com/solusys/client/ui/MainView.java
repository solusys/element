package com.solusys.client.ui;

public class MainView extends MainLayout {

	public MainView() {
		super();

		NavigationBar navigationBar = new NavigationBar(this.getRightCell());
		navigationBar.addComponent("Home", new HomeComponent());
		navigationBar.addComponent("About", new AboutComponent());
		navigationBar.addComponent("Contact", new ContactComponent());
		navigationBar.addComponent("Contacts", new ContactListComponent());
		navigationBar.init();
		getLeftCell().append(navigationBar.getElement());

	}
}
