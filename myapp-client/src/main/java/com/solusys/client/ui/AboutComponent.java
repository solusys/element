package com.solusys.client.ui;

import com.solusys.client.ui.router.AbstractRouteComponent;

public class AboutComponent extends AbstractRouteComponent {

	public AboutComponent() {
		super(Constants.ABOUT);
		element.textContent = "About Component";
	}

}
