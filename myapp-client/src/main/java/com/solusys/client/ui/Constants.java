package com.solusys.client.ui;

public class Constants {

	public static String HOME = "/";
	public static String ABOUT = "/about";
	public static String CONTACT = "/contact";
	public static String CONTACT_LIST = "/contacts";

}
