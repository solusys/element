package com.solusys.client.ui.components;

import elemental2.dom.CustomEventInit;
import elemental2.dom.Event;
import jsinterop.annotations.JsConstructor;

public class PageChangedEvent extends Event {
	public static final String TYPE = "pagechanged";
	// private int currentPage;
	private PageInfo pageInfo;

	@JsConstructor
	public PageChangedEvent(int currentPage, int rowsPerPage, String sortBy, boolean ascending) {
		super(TYPE, CustomEventInit.create());
		this.pageInfo = new PageInfo();
		this.pageInfo.setAscending(ascending);
		this.pageInfo.setPageNumber(currentPage);
		this.pageInfo.setPageSize(rowsPerPage);
		this.pageInfo.setSortBy(sortBy);
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}
}
