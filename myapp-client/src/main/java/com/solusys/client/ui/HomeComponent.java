package com.solusys.client.ui;

import com.solusys.client.ui.router.AbstractRouteComponent;

public class HomeComponent extends AbstractRouteComponent {
	
	public HomeComponent() {
		super(Constants.HOME);
		getElement().textContent = "Home Component";
	}
}
