package com.solusys.client.ui.components;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import ColumnSelectedEvent.EventOrigin;
import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.EventListener;
import elemental2.dom.HTMLTableCaptionElement;
import elemental2.dom.HTMLTableCellElement;
import elemental2.dom.HTMLTableElement;
import elemental2.dom.HTMLTableRowElement;

/**
 * The Table class is a generic implementation of a table with customizable
 * columns, sorting, and pagination. The class takes a list of rows and
 * generates an HTML table element for rendering.
 *
 * @param <T> The type of data contained in the table rows.
 * @author Adil Dalli
 */
public class Table<T> {
	private HTMLTableElement table;
	private Element paginationContainer;
	private Element pageCount;
	private final List<T> rows;
	private final List<String> columns;
	private final List<Function<T, String>> columnFunctions;
	private boolean multiSelect;
	private List<RowSelectedEventListener<T>> rowSelectedEventListeners;
	private static final int DOUBLE_CLICK_DELAY = 250; // milliseconds
	private int currentPage;
	private int rowsPerPage;
	private List<PageChangedEventListener> pageChangedEventListeners;
	private boolean ascending;
	private int columnIndex;
	private String pageCaption = "Page ";
	private String ofCaption = " of ";

	/**
	 * Creates an HTML table element using the provided rows and column
	 * configurations.
	 * 
	 * @return An HTML table element with configured columns and rows.
	 */
	public Table(List<T> rows) {
		this.rows = rows;
		this.columns = new ArrayList<>();
		this.columnFunctions = new ArrayList<>();
		this.multiSelect = false;
		this.rowSelectedEventListeners = new ArrayList<>();
		this.pageChangedEventListeners = new ArrayList<>();
		this.currentPage = 0;
		this.rowsPerPage = 10; // You can change this to your desired value
		this.ascending = true;
		this.columnIndex = 0;
	}

	/**
	 * Creates an HTML table element using the provided rows and column
	 * configurations.
	 * 
	 * @return An HTML table element with configured columns and rows.
	 */
	public Element createTableElement() {
		table = (HTMLTableElement) DomGlobal.document.createElement("table");
		table.setAttribute("class", "contact-table");

		HTMLTableCaptionElement caption = (HTMLTableCaptionElement) DomGlobal.document.createElement("caption");
		table.appendChild(caption);

		HTMLTableRowElement headerRow = (HTMLTableRowElement) DomGlobal.document.createElement("tr");

		for (int i = 0; i < columns.size(); i++) {
			int index = i;
			HTMLTableCellElement headerCell = (HTMLTableCellElement) DomGlobal.document.createElement("th");
			headerCell.textContent = columns.get(i);
			headerCell.addEventListener("click", (EventListener) event -> {
				event.stopPropagation(); // Stop event propagation for header cell
				ascending = !headerCell.classList.contains("ascending");
				columnIndex = index;
				headerCell.classList.toggle("ascending");
				firePageChangedEvent();
			});
			headerRow.appendChild(headerCell);
		}

		table.appendChild(headerRow);

		createPaginationElement();

		addSortedRowElements();

		return table;
	}

	private void addSortedRowElements() {

		// Implement sorting functionality

		rows.sort((row1, row2) -> {
			String cellValue1 = columnFunctions.get(columnIndex).apply(row1);
			String cellValue2 = columnFunctions.get(columnIndex).apply(row2);
			return ascending ? cellValue1.compareTo(cellValue2) : cellValue2.compareTo(cellValue1);
		});

		// Remove all current row elements
		while (table.rows.length > 1) {
			table.deleteRow(1);
		}

		for (T row : rows) {
			HTMLTableRowElement tableRow = (HTMLTableRowElement) DomGlobal.document.createElement("tr");

			handleClickEvent(tableRow, row);

			for (Function<T, String> columnFunction : columnFunctions) {
				HTMLTableCellElement cell = (HTMLTableCellElement) DomGlobal.document.createElement("td");
				cell.textContent = columnFunction.apply(row);
				tableRow.appendChild(cell);
			}

			table.appendChild(tableRow);
		}

		updateTableRows(table);
	}

	/**
	 * Updates the table with the rows of the current page based on the pagination
	 * configuration.
	 * 
	 * @param table The HTMLTableElement that needs to be updated with the current
	 *              page rows.
	 */
	private void updateTableRows(HTMLTableElement table) {
		// Remove all current row elements
		while (table.rows.length > 1) {
			table.deleteRow(1);
		}

		// Add row elements for the current page
		int start = currentPage * rowsPerPage;
		int end = Math.min(start + rowsPerPage, rows.size());
		for (int i = start; i < end; i++) {
			T row = rows.get(i);
			HTMLTableRowElement tableRow = createTableRow(row);
			table.appendChild(tableRow);
		}

		// Update Page count in pagination element
		updatePageCount();
	}

	/**
	 * Creates an HTML table row element for the given row data.
	 * 
	 * @param row The row data to create the table row element for.
	 * @return The HTMLTableRowElement representing the row data.
	 */
	private HTMLTableRowElement createTableRow(T row) {
		HTMLTableRowElement tableRow = (HTMLTableRowElement) DomGlobal.document.createElement("tr");

		handleClickEvent(tableRow, row);

		for (Function<T, String> columnFunction : columnFunctions) {
			HTMLTableCellElement cell = (HTMLTableCellElement) DomGlobal.document.createElement("td");
			cell.textContent = columnFunction.apply(row);
			tableRow.appendChild(cell);
		}

		return tableRow;
	}

	/**
	 * Handles the click event on a table row, including single and double click
	 * handling, row selection, and firing row selected events.
	 * 
	 * @param tableRow The HTMLTableRowElement that the click event should be
	 *                 handled on.
	 * @param rowData  The row data associated with the table row element.
	 */
	private void handleClickEvent(HTMLTableRowElement tableRow, T rowData) {
		AtomicInteger clickCount = new AtomicInteger(0);

		tableRow.addEventListener("click", (EventListener) event -> {
			clickCount.incrementAndGet();
			DomGlobal.setTimeout((args) -> {
				if (clickCount.get() == 1) {
					fireRowSelectedEvent(new RowSelectedEvent<>(rowData, false));
				} else if (clickCount.get() > 1) {
					fireRowSelectedEvent(new RowSelectedEvent<>(rowData, true));
				}
				clickCount.set(0);
			}, DOUBLE_CLICK_DELAY);

			// Move the row selection logic outside the setTimeout function
			String selectedClass = "selected";
			if (multiSelect) {
				tableRow.classList.toggle(selectedClass);
			} else {
				HTMLTableElement table = (HTMLTableElement) tableRow.parentElement;
				for (int i = 0; i < table.rows.length; i++) {
					HTMLTableRowElement row = (HTMLTableRowElement) table.rows.item(i);
					if (row != tableRow) {
						row.classList.remove(selectedClass);
					}
				}
				tableRow.classList.add(selectedClass);
			}
		});
	}

	/**
	 * Sets the number of items to be displayed per page.
	 *
	 * @param itemsPerPage The number of items to be displayed per page.
	 */
	public void setItemsPerPage(int itemsPerPage) {
		this.rowsPerPage = itemsPerPage;
		updateTableRows(table);
	}

	/**
	 * Creates an HTML element for pagination at the bottom of the table.
	 *
	 * @return An HTML element containing the pagination controls.
	 */

	private Element createPaginationElement() {
		paginationContainer = DomGlobal.document.createElement("div");
		paginationContainer.setAttribute("class", "pagination-container");

		// Add page count
		pageCount = DomGlobal.document.createElement("span");
		pageCount.setAttribute("class", "page-count");

		// Add first button
		Element firstButton = DomGlobal.document.createElement("button");
		firstButton.setAttribute("class", "pagination-button");
		firstButton.textContent = "First";
		firstButton.addEventListener("click", (event) -> {
			if (currentPage > 0) {
				currentPage = 0;
				firePageChangedEvent();
			}
		});

		// Add previous button
		Element prevButton = DomGlobal.document.createElement("button");
		prevButton.setAttribute("class", "pagination-button");
		prevButton.textContent = "Previous";
		prevButton.addEventListener("click", (event) -> {
			if (currentPage > 0) {
				currentPage--;
				firePageChangedEvent();
			}
		});

		// Add next button
		Element nextButton = DomGlobal.document.createElement("button");
		nextButton.setAttribute("class", "pagination-button");
		nextButton.textContent = "Next";
		nextButton.addEventListener("click", (event) -> {
			if ((currentPage + 1) * rowsPerPage < rows.size()) {
				currentPage++;
				firePageChangedEvent();
			}
		});

		// Add last button
		Element lastButton = DomGlobal.document.createElement("button");
		lastButton.setAttribute("class", "pagination-button");
		lastButton.textContent = "Last";
		lastButton.addEventListener("click", (event) -> {
			int lastPage = (rows.size() - 1) / rowsPerPage;
			if (currentPage != lastPage) {
				currentPage = lastPage;
				firePageChangedEvent();
			}
		});

		// Add items per page
		Element itemsPerPage = DomGlobal.document.createElement("span");
		itemsPerPage.setAttribute("class", "items-per-page");
		itemsPerPage.textContent = "Items per page: " + rowsPerPage;

		paginationContainer.appendChild(firstButton);
		paginationContainer.appendChild(prevButton);
		paginationContainer.appendChild(pageCount);
		paginationContainer.appendChild(nextButton);
		paginationContainer.appendChild(lastButton);
		// paginationContainer.appendChild(itemsPerPage);

		return paginationContainer;
	}

	public void updatePageCount() {
		pageCount.textContent = pageCaption + (currentPage + 1) + ofCaption + ((rows.size() - 1) / rowsPerPage + 1);
	}

	public void addPageChangedEventListener(PageChangedEventListener listener) {
		pageChangedEventListeners.add(listener);
	}

	private void firePageChangedEvent() {

		DomGlobal.console.log("==> firePageChangedEvent");
		PageChangedEvent event = new PageChangedEvent(currentPage, rowsPerPage, columns.get(columnIndex), ascending);
		for (PageChangedEventListener listener : pageChangedEventListeners) {
			listener.onInvoke(event);
		}
	}

	/**
	 * Adds a row selected event listener.
	 * 
	 * @param listener The listener to be notified when a row is selected.
	 */

	public void addRowSelectedEventListener(RowSelectedEventListener<T> listener) {
		rowSelectedEventListeners.add(listener);
	}

	/**
	 * Fires the row selected event for all registered listeners.
	 * 
	 * @param event The event containing information about the selected row.
	 */
	private void fireRowSelectedEvent(RowSelectedEvent<T> event) {
		for (RowSelectedEventListener<T> listener : rowSelectedEventListeners) {
			listener.onInvoke(event);
		}
	}

	/**
	 * Adds a new column to the table with the specified name and function for
	 * extracting data from the row.
	 * 
	 * @param columnName     The name of the column to be displayed in the header.
	 * @param columnFunction The function used to extract data from the row object
	 *                       for the specified column.
	 */
	public void addColumn(String columnName, Function<T, String> columnFunction) {
		columns.add(columnName);
		columnFunctions.add(columnFunction);
	}

	/**
	 * Sets the table's row selection mode.
	 * 
	 * @param multiSelect If true, multiple rows can be selected; otherwise, only a
	 *                    single row can be selected at a time.
	 */
	public void setSelectionMode(boolean multiSelect) {
		this.multiSelect = multiSelect;
	}

	/**
	 * Adds a list of items to the table's data source.
	 *
	 * @param list A list of items to be added to the table.
	 */
	public void addItem(List<T> list) {
		rows.addAll(list);
		updateTableRows(table);
	}

	/**
	 * Retrieves all items from the table's data source.
	 *
	 * @return A list of all items currently in the table.
	 */
	public List<T> getItems() {
		return new ArrayList<>(rows);
	}

	/**
	 * Removes a list of items from the table's data source.
	 *
	 * @param list A list of items to be removed from the table.
	 */
	public void removeItems(List<T> list) {
		rows.removeAll(list);
		updateTableRows(table);
	}

	/**
	 * Removes all items from the table's data source.
	 */
	public void removeAllItems() {
		rows.clear();
		updateTableRows(table);
	}

	/**
	 * Replaces the table's data source with a new list of items.
	 *
	 * @param list A list of items to be set as the table's new data source.
	 */
	public void setItems(List<T> list) {
		rows.clear();
		rows.addAll(list);
		updateTableRows(table);
	}

	/**
	 * Retrieves the selected item(s) from the table based on the selection mode.
	 *
	 * @return A list of the selected item(s) in the table. If the selection mode is
	 *         single, the list will contain at most one item.
	 */
	public List<T> getSelectedItems() {
		List<T> selectedItems = new ArrayList<>();

		for (int i = 1; i < table.rows.length; i++) {
			HTMLTableRowElement row = (HTMLTableRowElement) table.rows.item(i);
			if (row.classList.contains("selected")) {
				int rowIndex = (currentPage * rowsPerPage) + i - 1;
				selectedItems.add(rows.get(rowIndex));
			}
		}

		return selectedItems;
	}

	public boolean isMultiSelect() {
		return multiSelect;
	}

	public void setMultiSelect(boolean multiSelect) {
		this.multiSelect = multiSelect;
	}

	public Element getPaginationContainer() {
		return paginationContainer;
	}

	public List<String> getColumns() {
		return columns;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public boolean isAscending() {
		return ascending;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public String getPageCaption() {
		return pageCaption;
	}

	public void setPageCaption(String pageCaption) {
		this.pageCaption = pageCaption;
	}

	public String getOfCaption() {
		return ofCaption;
	}

	public void setOfCaption(String ofCaption) {
		this.ofCaption = ofCaption;
	}

}
