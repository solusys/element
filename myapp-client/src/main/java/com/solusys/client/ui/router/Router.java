package com.solusys.client.ui.router;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

/**
 * The Router class is responsible for handling client-side routing in a web
 * application. It takes an HTML element representing the root of the
 * application and a list of Route objects that define the possible URL paths
 * and their corresponding handlers. When the Router is initialized, it
 * registers an event listener for changes in the URL path using the popstate
 * event. When the URL path changes, the handleUrlChange() method is called,
 * which matches the path to a Route and renders its content into the
 * application element. If no Route matches the path, a "404 Not Found" message
 * is displayed instead. The go(String path) method allows changing the URL path
 * programmatically by pushing a new entry to the browser history and calling
 * handleUrlChange() to update the content.
 * 
 * @author adildalli
 *
 */
public class Router {
	private final Element appElement;
	private final Map<String, AbstractRouteComponent> routes;

	public Router(Element appElement) {
		this.appElement = appElement;
		this.routes = new HashMap<String, AbstractRouteComponent>();
	}

	public void init() {
		DomGlobal.window.addEventListener("popstate", evt -> handleUrlChange());
		handleUrlChange();
	}

	/**
	 * uses reflection to check if the specified component class is annotated with
	 * RoutePath, and if so, adds a new Route to the routes list
	 * 
	 * @param viewName
	 * 
	 * @param componentClass
	 */
	public void addRoute(String viewName, AbstractRouteComponent componentClass) {
		routes.put(viewName, componentClass);
	}

	/**
	 * This method gets called whenever the URL changes. It uses the
	 * DomGlobal.window.location.pathname property to get the current URL path, and
	 * then iterates over the routes list that was passed to the Router instance.
	 * For each route, it checks if the route's path matches the current URL path.
	 * 
	 * If a matching route is found, the appElement is cleared of any content using
	 * appElement.innerHTML = "", and the route's render() method is called to
	 * render the appropriate content into the appElement. The method then returns.
	 * 
	 * If no matching route is found, a "404 Not Found" message is set as the
	 * textContent of the appElement.
	 */

	private void handleUrlChange() {
		String path = DomGlobal.window.location.pathname;
		for (Entry<String, AbstractRouteComponent> route : routes.entrySet()) {
			if (route.getValue().getPath().equals(path)) {
				appElement.innerHTML = "";
				route.getValue().render(appElement);
				return;
			}
		}
		appElement.textContent = "404 Not Found";
	}

	/**
	 * This method updates the browser's URL history using the pushState() method of
	 * the window.history object. This method takes three arguments: a state object
	 * (which can be used to store custom data), a title (which is currently ignored
	 * by most browsers), and the new URL path.
	 * 
	 * @param path
	 */
	public void go(String path) {
		DomGlobal.window.history.pushState(null, null, path);
		handleUrlChange();
	}

	public Map<String, AbstractRouteComponent> getRoutes() {
		return routes;
	}
}