package com.solusys.client.ui;

import com.solusys.client.ui.router.AbstractRouteComponent;

import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLButtonElement;
import elemental2.dom.HTMLFormElement;
import elemental2.dom.HTMLInputElement;
import elemental2.dom.HTMLLabelElement;

public class ContactComponent extends AbstractRouteComponent {

	private HTMLInputElement firstNameInput;
	private HTMLInputElement lastNameInput;
	private HTMLInputElement dobInput;
	private HTMLInputElement emailInput;

	public ContactComponent() {
		super(Constants.CONTACT);
		element = DomGlobal.document.createElement("div");
		element.textContent = "Contact Component";
		

		// Create form elements
		HTMLFormElement form = (HTMLFormElement) DomGlobal.document.createElement("form");
		form.setAttribute("class", "contact-form");
		// Use CSS to display form elements vertically
		form.style.display = "flex";
		form.style.flexDirection = "column";

		// Create input elements
		firstNameInput = (HTMLInputElement) DomGlobal.document.createElement("input");
		firstNameInput.setAttribute("type", "text");
		firstNameInput.setAttribute("name", "firstName");
		firstNameInput.setAttribute("required", "");

		lastNameInput = (HTMLInputElement) DomGlobal.document.createElement("input");
		lastNameInput.setAttribute("type", "text");
		lastNameInput.setAttribute("name", "lastName");
		lastNameInput.setAttribute("required", "");

		dobInput = (HTMLInputElement) DomGlobal.document.createElement("input");
		dobInput.setAttribute("type", "date");
		dobInput.setAttribute("name", "dob");
		dobInput.setAttribute("required", "");

		emailInput = (HTMLInputElement) DomGlobal.document.createElement("input");
		emailInput.setAttribute("type", "email");
		emailInput.setAttribute("name", "email");
		emailInput.setAttribute("required", "");

		// Create labels for input elements
		HTMLLabelElement firstNameLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
		firstNameLabel.setAttribute("for", "firstName");
		firstNameLabel.textContent = "First Name";

		HTMLLabelElement lastNameLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
		lastNameLabel.setAttribute("for", "lastName");
		lastNameLabel.textContent = "Last Name";

		HTMLLabelElement emailLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
		emailLabel.setAttribute("for", "email");
		emailLabel.textContent = "Email";

		// Create submit button
		HTMLButtonElement submitButton = (HTMLButtonElement) DomGlobal.document.createElement("button");
		submitButton.textContent = "Save";
		// submitButton.addEventListener("click", event -> save(event));

		// Add form elements to form
		form.appendChild(firstNameLabel);
		form.appendChild(firstNameInput);
		form.appendChild(lastNameLabel);
		form.appendChild(lastNameInput);
		form.appendChild(dobInput);
		form.appendChild(emailLabel);
		form.appendChild(emailInput);
		form.appendChild(submitButton);

		// Add form to document
		element.appendChild(form);

	}
}