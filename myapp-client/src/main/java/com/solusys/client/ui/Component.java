package com.solusys.client.ui;

import elemental2.dom.Element;

public interface Component {
	Element getElement();
}