package com.solusys.client;

import com.solusys.client.ui.MainLayout;
import com.solusys.client.ui.MainView;

import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLMetaElement;

//import elemental2.dom.DomGlobal;
//import elemental2.dom.Event;
//import elemental2.dom.HTMLButtonElement;
//import elemental2.dom.HTMLFormElement;
//import elemental2.dom.HTMLInputElement;
//import elemental2.dom.HTMLLabelElement;
//import elemental2.dom.Response;
//import elemental2.promise.Promise;
//import jsinterop.base.Js;

public class app {

//	private HTMLInputElement firstNameInput;
//	private HTMLInputElement lastNameInput;
//	private HTMLInputElement dobInput;
//	private HTMLInputElement emailInput;

	public void onModuleLoad() {

		// Set the viewport meta tag to fix errors

		// Create and show the main layout

		new MainView();
	}

//	public void onModuleLoad1() {
//
//		// Create form elements
//		HTMLFormElement form = (HTMLFormElement) DomGlobal.document.createElement("form");
//		form.setAttribute("class", "contact-form");
//		// Use CSS to display form elements vertically
//		form.style.display = "flex";
//		form.style.flexDirection = "column";
//
//		// Create input elements
//		firstNameInput = (HTMLInputElement) DomGlobal.document.createElement("input");
//		firstNameInput.setAttribute("type", "text");
//		firstNameInput.setAttribute("name", "firstName");
//		firstNameInput.setAttribute("required", "");
//
//		lastNameInput = (HTMLInputElement) DomGlobal.document.createElement("input");
//		lastNameInput.setAttribute("type", "text");
//		lastNameInput.setAttribute("name", "lastName");
//		lastNameInput.setAttribute("required", "");
//
//		dobInput = (HTMLInputElement) DomGlobal.document.createElement("input");
//		dobInput.setAttribute("type", "date");
//		dobInput.setAttribute("name", "dob");
//		dobInput.setAttribute("required", "");
//
//		emailInput = (HTMLInputElement) DomGlobal.document.createElement("input");
//		emailInput.setAttribute("type", "email");
//		emailInput.setAttribute("name", "email");
//		emailInput.setAttribute("required", "");
//
//		// Create labels for input elements
//		HTMLLabelElement firstNameLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
//		firstNameLabel.setAttribute("for", "firstName");
//		firstNameLabel.textContent = "First Name";
//
//		HTMLLabelElement lastNameLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
//		lastNameLabel.setAttribute("for", "lastName");
//		lastNameLabel.textContent = "Last Name";
//
//		HTMLLabelElement dobLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
//		dobLabel.setAttribute("for", "dob");
//		dobLabel.textContent = "Date of Birth";
//
//		HTMLLabelElement emailLabel = (HTMLLabelElement) DomGlobal.document.createElement("label");
//		emailLabel.setAttribute("for", "email");
//		emailLabel.textContent = "Email";
//
//		// Create submit button
//		HTMLButtonElement submitButton = (HTMLButtonElement) DomGlobal.document.createElement("button");
//		submitButton.textContent = "Save";
//		submitButton.addEventListener("click", event -> save(event));
//
//		// Add form elements to form
//		form.appendChild(firstNameLabel);
//		form.appendChild(firstNameInput);
//		form.appendChild(lastNameLabel);
//		form.appendChild(lastNameInput);
//		form.appendChild(dobLabel);
//		form.appendChild(dobInput);
//		form.appendChild(emailLabel);
//		form.appendChild(emailInput);
//		form.appendChild(submitButton);
//
//		// Add form to document
//		DomGlobal.document.body.appendChild(form);
//
//	}
//
//	private Object save(Event event) {
//		return null;
//	}
//
//	private void goGetData(HTMLButtonElement button, String str) {
//		DomGlobal.fetch("/hello.json?name=" + str).then(Response::json).then(json -> {
//			String string = Js.asPropertyMap(json).getAsAny("response").asString();
//			button.textContent = string;
//			return Promise.resolve(json);
//		});
//	}
}
